void main() {
  const double netSalary = 5000.66;
  const double expenses = 3500.45;

  if (netSalary > expenses) {
    print(
        'You have saved \$${(netSalary - expenses).toStringAsFixed(2)} this month');
  } else if (netSalary < expenses) {
    print(
        'You have lost \$${(netSalary - expenses).toStringAsFixed(2)} this month');
  } else {
    print('You balance hasn\'t changed');
  }
}
