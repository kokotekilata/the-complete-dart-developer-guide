enum Medal {
  gold,
  silver,
  bronze,
  noMedal,
}

void main() {
  Medal medal = Medal.gold;
  switch (medal) {
    case Medal.gold:
      print('gold 🤩');
      break;
    case Medal.silver:
      print('silver 😃');
      break;
    case Medal.bronze:
      print('bronze 🙂');
      break;
    case Medal.noMedal:
      print('no medal, try again 😢');
      break;
  }

  // const pos = 1;

  // switch (pos) {
  //   case 1:
  //     print('gold 🤩');
  //     break;
  //   case 2:
  //     print('silver 😃');
  //     break;
  //   case 3:
  //     print('bronze 🙂');
  //     break;
  //   default:
  //     print('no medal, try again 😢');
  //     break;
  // }
}
