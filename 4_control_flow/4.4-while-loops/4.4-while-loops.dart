void main() {
  var i = 1;
  while (i <= 5) {
    print('x' * i); // String multiplication: concatenates a string N times
    i++;
  }
  print('Done');
}
