void main() {
  String name = 'Andrea';
  print(name.toUpperCase());

  // Dart is Statically typed
  // It enforces strong guarantees at compile time

  // The type checker will throw an error during compilation of the code bellow:
  // int name = 'Andrea';
  // print(name.toUpperCase());

  // The type checker will throw an error during compilation of the code bellow:
  // name = 10; // change of type
}
