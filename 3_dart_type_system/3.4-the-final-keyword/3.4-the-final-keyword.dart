void main() {
  final String name = 'Bosco';
  final age = 36;
  // age = 37; // Limiting the states - can't altered once initiliazed
  final height = 1.84;

  final String title = 'Dart course';
  final titleUppercased = title.toUpperCase();
  print(titleUppercased);
}
