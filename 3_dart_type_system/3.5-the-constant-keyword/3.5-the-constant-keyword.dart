void main() {
  const title = 'Dart course'; // const are compile-time constants
  final titleUppercased = title.toUpperCase();
  print(titleUppercased);

  const x = 1;
  const y = 2;
  const z = x + y; // Can be evaluated at compile-time
}
