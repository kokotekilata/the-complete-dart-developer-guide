void main() {
  // Using type inference:
  // https://dart.dev/guides/language/type-system#type-inference
  var name = 'Andrea';

  // Throws an error as the variable type was already infered and
  // can't be changed
  // name = 10;
  var age = 36;
}
