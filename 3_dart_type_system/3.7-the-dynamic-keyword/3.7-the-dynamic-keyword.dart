void main() {
  var x = 10;
  // x = true; // Can't change type

  dynamic y = 10; // Allows the variable to take any type
  y = true;
}
