void main() {
  String firstName = 'Marco';
  String lastName = 'Rodriguez';
  int age = 29;
  double height = 1.79;

  print("My name is " + firstName + " " + lastName);
  print("I am $age years old and $height meters tall");
  print("Next year I will be ${age + 1} years old");
}
