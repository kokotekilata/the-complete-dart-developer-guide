void main() {
  print(5 == 2); // equal
  print(5 != 2); // not equal
  print(5 >= 2); // greater or equal to
  print(5 > 2); // greater than
  print(5 <= 2); // less or equal to
  print(5 < 2); // less than

  print(5 < 2 || 5 < 7); // logical OR
  print(5 < 2 && 5 < 7); // local AND

  print((5 < 2 || 5 < 7) && 5 != 6); // combination of different operations

  print(!(5 < 2)); // NOT operator

  // Practical example
  String email = 'test@example.com';
  print(email.isNotEmpty && email.contains('@'));
}
