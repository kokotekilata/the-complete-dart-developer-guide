void main() {
  // expressions - hold a value at runtime
  int x = (10 + 3) % 4;
  print(x); // statement
  // statements do NOT hold a value
}
