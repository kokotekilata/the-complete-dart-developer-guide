void main() {
  print(5 + 2);
  print(5 - 2);
  print(5 * 2);
  print(5 / 2);

  print(5 ~/ 2); // Integer division

  print(5 % 2); // Modulo operator - get remainder

  int x = 5;
  x = x + 2;
  print(x);

  int y = 5;
  y += 2;
  print(y);

  int w = 5;
  w ~/= 2; // int augmented division operator
  print(w);

  double u = 5;
  u /= 2; // double augmented division operator - u has t be of type double
  print(u);

  print(10 - 2 * 3);
}
