void main() {
  String title = 'Dart course';
  print(title);

  print(title.toUpperCase());
  print(title.toLowerCase());

  String title2 = 'Dart course'.toUpperCase();
  print(title2);
}
