void main() {
  print('This is a short sentence.'
      'This is a longer sentence, I dare say'
      'This is an even longer sentence, which will not fit inside a single line.');

  print('This is a short sentence.\n'
      'This is a longer sentence, I dare say\n'
      'This is an even longer sentence, which will not fit inside a single line.\n');

  print("""
This is a short sentence.
This is a longer sentence, I dare say
This is an even longer sentence, which will not fit inside a single line.
  """);

  String s = """
This is a short sentence.
This is a longer sentence, I dare say
This is an even longer sentence, which will not fit inside a single line.
  """;
  print('$s');
}
