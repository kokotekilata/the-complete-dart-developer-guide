void main() {
  int x = 1;
  // Postfix increment operator:
  // First assign x to y
  // Then increement x
  int y = x++;
  print('x: $x, y: $y');

  // Prefix increment operator:
  // First increment x
  // Then assign x to w
  int w = ++x;
  print('x: $x, w: $w');
}
