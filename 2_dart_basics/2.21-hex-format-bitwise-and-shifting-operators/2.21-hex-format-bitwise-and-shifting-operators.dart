void main() {
  int green = 0xFF00FF00;

  int x = 0xF0; // binary: 11110000
  int y = 0x0F; // binary: 00001111

  print(x | y);
  print((x | y).toRadixString(16)); // bitwise OR
  print((x | y).toRadixString(2));

  print((x & y).toRadixString(2)); // bitwise AND
  print((x ^ y).toRadixString(2)); // bitwise XOR

  print((~y).toRadixString(2)); // bitwise NOT

  print((x << 2).toRadixString(2)); // bitwise shift left
  print((x >> 2).toRadixString(2)); // bitwise shift right
}
