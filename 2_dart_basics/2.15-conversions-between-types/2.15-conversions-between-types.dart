void main() {
  int age = 36;

  String ageString = age.toString();
  print(ageString);

  double height = 1.86;
  String heightString = height.toStringAsFixed(1);
  print(heightString);

  String ratingString = '4.5';
  double rating = double.parse(ratingString);
  print(rating);

  // Example where the conversion fails.
  // Throws: Unhandled exception
  // String helloString = 'hello';
  // print(double.parse(helloString));

  int x = 10;
  double y = x.toDouble();
  double z = 20;
  int w = 40.6.round();
  print('Double y:$y Double z:$z int w:$w');
}
