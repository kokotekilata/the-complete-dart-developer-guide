# The Complete Dart Developer Guide 

Going over [Anrea Bizzoto's Dart Course](https://codewithandrea.com/)

# Lessons:
1. Introduction
2. Dart Basics
3. Dart type system
4. Control flow
5. Project: Building a command line app
6. Collections
7. Project: Data Processing in Dart
8. Dart Null Safety
9. Functions: Basics
10. Functions: Advanced
11. Classes: Basics
12. Classes: Advanced
13. Project: Simple eCommerce
14. Mixins & Extensions
15. Error Handling & Exceptions
16. Async Programming
17. Weather App
